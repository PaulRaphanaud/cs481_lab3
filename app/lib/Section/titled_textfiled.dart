import 'package:app/Widget/padded_text.dart';
import 'package:flutter/material.dart';

class TitledTextField extends StatefulWidget {
  TitledTextField({Key key, this.title, this.numeric, this.controller})
      : super(key: key);

  @required
  final String title;
  @required
  final bool numeric;
  @required
  final TextEditingController controller;

  @override
  _TitledTextFieldState createState() => _TitledTextFieldState();
}

class _TitledTextFieldState extends State<TitledTextField> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        PaddedText(padding: 20.0, data: widget.title),
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: TextField(
            keyboardType:
                (widget.numeric) ? TextInputType.number : TextInputType.text,
            controller: widget.controller,
          ),
        ),
        SizedBox(
          height: 50,
        ),
      ],
    );
  }
}
