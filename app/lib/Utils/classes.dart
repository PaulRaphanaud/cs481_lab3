import './type_expenses.dart';

class Expense {
  String name;
  DateTime date;
  TypeExpenses type;
  double amount;

  Expense(this.name, this.date, this.type, this.amount);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['date'] = this.date;
    data['type'] = this.type;
    data['amount'] = this.amount;
    return data;
  }
}
