enum TypeExpenses {
  shopping,
  food,
  activities,
  house,
}

String expenseName(TypeExpenses type) {
  switch (type) {
    case TypeExpenses.shopping:
      return "Shopping";
    case TypeExpenses.food:
      return "Food";
    case TypeExpenses.activities:
      return "Activities";
    case TypeExpenses.house:
      return "House";
    default:
      return "Undefined";
  }
}

int expenseIndex(TypeExpenses type) {
  switch (type) {
    case TypeExpenses.shopping:
      return 0;
    case TypeExpenses.food:
      return 1;
    case TypeExpenses.activities:
      return 2;
    case TypeExpenses.house:
      return 3;
    default:
      return 4;
  }
}
