import 'package:flutter/material.dart';

class PaddedText extends StatelessWidget {
  PaddedText({Key key, this.padding, this.data}) : super(key: key);

  @required
  final String data;
  @required
  final double padding;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(padding),
      child: Text(data),
    );
  }
}
