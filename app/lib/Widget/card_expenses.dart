import 'package:app/Utils/classes.dart';
import 'package:app/Utils/type_expenses.dart';
import 'package:flutter/material.dart';

class CardExpenses extends StatelessWidget {
  CardExpenses({Key key, this.expense}) : super(key: key);

  @required
  final Expense expense;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      child: ListTile(
        leading: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("${expense.date.day.toString()}",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500)),
            Text("${expense.date.month.toString()}",
                style: TextStyle(fontSize: 16)),
          ],
        ),
        title: Text(expenseName(expense.type)),
        subtitle: Text(expense.name),
        trailing: SizedBox(
          width: 100,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(expense.amount.toString()),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: Icon(Icons.attach_money_rounded,
                    color: Colors.green, size: 20),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
