import 'package:app/Page/add_expenses_page.dart';
import 'package:app/Utils/classes.dart';
import 'package:flutter/material.dart';
import '../Utils/type_expenses.dart';

class TableExpenses extends StatefulWidget {
  TableExpenses(this.expenses, this.addExpense, {Key key}) : super(key: key);

  final List<Expense> expenses;
  final Function addExpense;

  @override
  _TableExpensesState createState() => _TableExpensesState();
}

class _TableExpensesState extends State<TableExpenses> {
  List<DataRow> _rows;
  List<DataColumn> _columns;

  DataColumn newColumn(String labelColumn, bool isNumeric) {
    return DataColumn(label: Text(labelColumn,
      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
      numeric: isNumeric,
    );
  }

  DataRow newRow(Expense expense) {
    print(expense.date.day.toString());
    return DataRow(cells: [
      DataCell(Text(
          "${expense.date.year.toString()}-${expense.date.month.toString()}-${expense.date.day.toString()}")),
      DataCell(Text(expense.name)),
      DataCell(Text(expenseName(expense.type))),
      DataCell(Text(expense.amount.toString()))
    ]);
  }

  @override
  void initState() {
    _rows = List<DataRow>();
    _columns = List<DataColumn>();
    _columns.add(newColumn("Date", false));
    _columns.add(newColumn("Name", false));
    _columns.add(newColumn("Type", false));
    _columns.add(newColumn("Expense", true));

    if (widget.expenses != null)
      widget.expenses.forEach((e) => _rows.add(newRow(e)));

    super.initState();
  }

  void addRow(Expense expense) {
    setState(() {
      _rows.add(newRow(expense));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: DataTable(
                rows: _rows,
                columns: _columns,
              ))),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add_box),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => AddExpenses()),
            ).then((value) {
              addRow(Expense(value[0], DateTime.now(), value[1], value[2]));
              widget.addExpense(
                  Expense(value[0], DateTime.now(), value[1], value[2]));
            });
          }),
    );
  }
}
