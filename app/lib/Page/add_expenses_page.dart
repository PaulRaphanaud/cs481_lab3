import 'package:app/Section/titled_textfiled.dart';
import 'package:app/Utils/type_expenses.dart';
import 'package:app/Widget/padded_text.dart';
import 'package:flutter/material.dart';

class AddExpenses extends StatefulWidget {
  @override
  _AddExpensesState createState() => _AddExpensesState();
}

class _AddExpensesState extends State<AddExpenses> {
  TextEditingController _controllerName;
  TextEditingController _controllerAmount;
  TypeExpenses _choosenExpenses;
  String _errorMessage;

  @override
  void initState() {
    _controllerName = TextEditingController();
    _controllerAmount = TextEditingController();

    super.initState();
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    _controllerName.dispose();
    _controllerAmount.dispose();
    super.dispose();
  }

  void addExpense() {
    // Error handling on controller
    if (_controllerName.text == "") {
      setState(() {
        _errorMessage = "Empty name";
      });
      return;
    }
    double amount = double.tryParse(_controllerAmount.text) ?? -1;
    if (amount == -1) {
      setState(() {
        _errorMessage = "Insert a valid amount";
      });
      return;
    }
    _errorMessage = null;
    Navigator.pop(context, [
      _controllerName.text,
      _choosenExpenses,
      amount,
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text(
          "Add an expenses",
          style: TextStyle(color: Colors.black),
        ),
        elevation: 0.0,
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 100,
            ),
            TitledTextField(
              title: "Name :",
              numeric: false,
              controller: _controllerName,
            ),
            TitledTextField(
              title: "Amount (number):",
              numeric: true,
              controller: _controllerAmount,
            ),
            PaddedText(padding: 20.0, data: "Choose type :"),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Center(
                child: DropdownButton<TypeExpenses>(
                  isExpanded: false,
                  value: _choosenExpenses,
                  elevation: 0,
                  style: TextStyle(color: Colors.black),
                  onChanged: (TypeExpenses newValue) {
                    setState(() {
                      _choosenExpenses = newValue;
                    });
                  },
                  items: TypeExpenses.values
                      .map<DropdownMenuItem<TypeExpenses>>(
                          (TypeExpenses value) {
                    return DropdownMenuItem<TypeExpenses>(
                      value: value,
                      child: Container(
                          height: 50,
                          width: 150,
                          child: Center(child: Text(expenseName(value)))),
                    );
                  }).toList(),
                ),
              ),
            ),
            if (_errorMessage != null)
              Center(
                child: Text(
                  _errorMessage,
                  style: TextStyle(color: Colors.redAccent),
                ),
              ),
            Spacer(
              flex: 1,
            ),
            Center(
                child: OutlinedButton(
                    onPressed: addExpense, child: Text("Submit"))),
          ],
        ),
      ),
    );
  }
}
