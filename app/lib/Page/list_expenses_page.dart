import 'package:app/Utils/classes.dart';
import 'package:app/Utils/type_expenses.dart';
import 'package:app/Widget/card_expenses.dart';
import 'package:flutter/material.dart';

class ListExpenses extends StatefulWidget {
  ListExpenses(this.expenses, {Key key}) : super(key: key);

  final List<Expense> expenses;

  @override
  _ListExpensesState createState() => _ListExpensesState();
}

class _ListExpensesState extends State<ListExpenses> {
  List<bool> _selected = [false, false, false, false];

  bool checkNoSelected() {
    bool isSelected = true;

    _selected.forEach((isTrue) {
      if (isTrue == true) {
        isSelected = false;
      }
    });
    return isSelected;
  }

  bool checkSelected(Expense expense) {
    return _selected[expenseIndex(expense.type)];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            height: 70,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: TypeExpenses.values.map<Widget>((TypeExpenses value) {
                return DropdownMenuItem<TypeExpenses>(
                    value: value,
                    child: Padding(
                        padding: EdgeInsets.all(10),
                        child: FilterChip(
                          selected: _selected[expenseIndex(value)],
                          selectedColor: Colors.green,
                          label: Text(expenseName(value)),
                          onSelected: (selected) {
                            setState(() {
                              _selected[expenseIndex(value)] = selected;
                            });
                          },
                        )));
              }).toList(),
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: widget.expenses.length,
              itemBuilder: (context, index) {
                Expense expense = widget.expenses[index];
                if (checkNoSelected() == true) {
                  print("AZEAZEAZE");
                  return CardExpenses(expense: expense);
                } else {
                  if (checkSelected(expense))
                    return CardExpenses(expense: expense);
                  else
                    return Container();
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
