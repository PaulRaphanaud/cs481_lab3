import 'package:app/Page/list_expenses_page.dart';
import 'package:app/Page/table_expenses_page.dart';
import 'package:app/Utils/classes.dart';
import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  PageController _controller;
  List<Expense> _expenses;

  @override
  initState() {
    _controller = PageController();
    _expenses = List<Expense>();
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void addExpense(Expense newExpense) {
    setState(() {
      _expenses.add(newExpense);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: PageView(
        controller: _controller,
        children: [TableExpenses(_expenses, this.addExpense), ListExpenses(_expenses)],
      ),
    );
  }
}
